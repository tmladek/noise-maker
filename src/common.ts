enum LoadingState {
  UNLOADED = "unloaded",
  LOADING = "loading",
  LOADED = "loaded",
}

enum PlayingState {
  STOPPED = "stopped",
  PLAYING = "playing"
}

export {LoadingState, PlayingState};

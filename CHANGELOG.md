# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.3] - 2020-01-12
### Fixed
- Fixed volume actually not getting animated after first slider movement.

## [0.1.2] - 2020-01-12
### Added
- Slider styling

### Changed
- Slider now controls *maximum* volume independent of animated volume (i.e. acts as a volume fader) 

## [0.1.1] - 2020-01-12
### Added
- Clear button for URLs.
- Basic layout responsiveness.

### Changed
- Move channel title to top.

## [0.1.0] - 2020-01-11
### Added
- First usable version - single animation mode yet; YouTube only.

[Unreleased]: https://gitlab.com/tmladek/noise-maker/compare/v0.1.3...master
[0.1.3]: https://gitlab.com/tmladek/noise-maker/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/tmladek/noise-maker/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/tmladek/noise-maker/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/tmladek/noise-maker/-/tags/v0.1.0
